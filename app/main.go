package main

import (
	"belajar/exam-skyshi/lib/db"
	"belajar/exam-skyshi/service/config"
	"belajar/exam-skyshi/service/handler"
	"belajar/exam-skyshi/service/repo"
	"belajar/exam-skyshi/service/usecase"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func main() {
	route := gin.Default()
	set := config.Config{}
	set.CatchError(set.InitEnv())
	Database := set.GetDBConfig()
	Apps := set.GetApp()
	db, err := db.ConnectiontoMYSQL(Database)
	if err != nil {
		log.Println(err)
		return
	}

	UserRepo := repo.NewRepoActivity(db)
	UserUsecase := usecase.NewUsecaseActivity(UserRepo)

	RepoTodo := repo.NewRepoTodo(db)
	TodoUsecase := usecase.NewUsecaseTodo(RepoTodo)

	HanderActivity := handler.NewHandlerActivity(UserUsecase)
	HandlerTodo := handler.NewHandlerTodo(TodoUsecase)

	//Activity
	route.POST("/activity-groups", HanderActivity.ActivityCreate)
	route.GET("/activity-groups", HanderActivity.GetAllDetailActivity)
	route.GET("/activity-groups/:id", HanderActivity.GetOneActivity)
	route.DELETE("/activity-groups/:id", HanderActivity.DeleteActivity)
	route.PATCH("/activity-groups/:id", HanderActivity.UpdateActivity)

	//Todo
	route.POST("/todo-items", HandlerTodo.CreateTodo)
	route.GET("/todo-items", HandlerTodo.GetAllTodoDetail)
	route.GET("/todo-items/:id", HandlerTodo.GetTodoById)
	route.DELETE("/todo-items/:id", HandlerTodo.DeleteTodo)
	route.PATCH("/todo-items/:id", HandlerTodo.UpdateTodo)
	Addrs := fmt.Sprintf(":%s", Apps.Port)

	server := http.Server{
		Addr:    Addrs,
		Handler: route,
	}
	server.ListenAndServe()
	if err != nil {
		return
	}

}
