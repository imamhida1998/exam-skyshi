package handler

import (
	"belajar/exam-skyshi/service/helpers"
	"belajar/exam-skyshi/service/model/request"
	"belajar/exam-skyshi/service/usecase"
	"fmt"
	"github.com/gin-gonic/gin"

	"net/http"
)

type HandlerActivity struct {
	userUsecase usecase.UsercaseActivity
}

func NewHandlerActivity(user usecase.UsercaseActivity) *HandlerActivity {
	return &HandlerActivity{user}
}

func (h *HandlerActivity) ActivityCreate(c *gin.Context) {
	var input request.CreateActivity

	err := c.ShouldBindJSON(&input)
	if err != nil {
		MessageError := gin.H{"errors": err.Error()}
		c.JSON(http.StatusBadRequest, MessageError)
		return
	}

	res, err := h.userUsecase.CreatedActivty(input)
	if err != nil {
		MessageError := gin.H{"errors": err}
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (h *HandlerActivity) GetAllDetailActivity(c *gin.Context) {

	res, err := h.userUsecase.GetAllActivity()
	if err != nil {
		MessageError := gin.H{"errors": err}
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (h *HandlerActivity) GetOneActivity(c *gin.Context) {
	var input request.ActivityId

	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := h.userUsecase.GetOneActivityId(input)
	if err != nil {
		MessageError := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	if res.Id == 0 {
		c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", nil))
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (h *HandlerActivity) DeleteActivity(c *gin.Context) {
	var input request.ActivityId

	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = h.userUsecase.DeleteActivity(input)
	if err != nil {
		MessageError := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	response := fmt.Sprintf("Activity with ID %d Not Found", input.Id)
	c.JSON(http.StatusCreated, helpers.FormatResponse("Not Found", response))

}

func (h *HandlerActivity) UpdateActivity(c *gin.Context) {
	var inputUri request.ActivityId
	var update request.Activity

	err := c.ShouldBindUri(&inputUri)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = c.ShouldBindJSON(&update)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = h.userUsecase.UpdateActivity(inputUri, update)
	if err != nil {
		MessageError := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	res, err := h.userUsecase.GetOneActivityId(inputUri)
	if err != nil {
		MessageError := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	MessageError := helpers.FormatDataResponse("Success", "Success", res)
	c.JSON(http.StatusOK, MessageError)

}
