package handler

import (
	"belajar/exam-skyshi/service/helpers"
	"belajar/exam-skyshi/service/model/request"
	"belajar/exam-skyshi/service/usecase"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type HandlerTodo struct {
	TodoCase usecase.UsercaseTodo
}

func NewHandlerTodo(todo usecase.UsercaseTodo) *HandlerTodo {
	return &HandlerTodo{todo}
}

func (t *HandlerTodo) CreateTodo(c *gin.Context) {
	var input request.CreateTodo

	err := c.ShouldBindJSON(&input)
	if err != nil {
		MessageError := gin.H{"errors": err.Error()}
		c.JSON(http.StatusBadRequest, MessageError)
		return
	}

	res, err := t.TodoCase.CreatedTodo(input)
	if err != nil {
		MessageError := gin.H{"errors": err}
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (t *HandlerTodo) GetAllTodoDetail(c *gin.Context) {
	res, err := t.TodoCase.GetAllTodo()
	if err != nil {
		MessageError := helpers.FormatResponse("Error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (t *HandlerTodo) GetTodoById(c *gin.Context) {
	var input request.TodoId

	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	res, err := t.TodoCase.GetOneTodo(input)
	if err != nil {
		MessageError := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	if res.Id == 0 {
		ResJson := fmt.Sprintf("Todo with ID %d Not Found", input.Id)
		c.JSON(http.StatusCreated, helpers.FormatResponse("Not Found", ResJson))
		return
	}

	c.JSON(http.StatusCreated, helpers.FormatDataResponse("Success", "Success", res))

}

func (t *HandlerTodo) DeleteTodo(c *gin.Context) {
	var input request.TodoId

	err := c.ShouldBindUri(&input)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = t.TodoCase.DeleteTodo(input)
	if err != nil {
		MessageError := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	response := fmt.Sprintf("Todo with ID %d Not Found", input.Id)
	c.JSON(http.StatusCreated, helpers.FormatResponse("Not Found", response))

}

func (t *HandlerTodo) UpdateTodo(c *gin.Context) {
	var inputUri request.TodoId
	var update request.UpdateTodo

	err := c.ShouldBindUri(&inputUri)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = c.ShouldBindJSON(&update)
	if err != nil {
		response := helpers.FormatDataResponse("error", "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	if update.Status != "ok" {
		response := helpers.FormatResponse("error", "if ok , please input status ok")
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = t.TodoCase.UpdateTodo(inputUri, update)
	if err != nil {
		MessageError := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	res, err := t.TodoCase.GetOneTodo(inputUri)
	if err != nil {
		MessageError := helpers.FormatResponse("error", err.Error())
		c.JSON(http.StatusInternalServerError, MessageError)
		return
	}

	MessageError := helpers.FormatDataResponse("Success", "Success", res)
	c.JSON(http.StatusOK, MessageError)

}
