package repo

import (
	"belajar/exam-skyshi/service/model"
	"belajar/exam-skyshi/service/model/request"
	"database/sql"
	"log"
)

type RepoTodo interface {
	CreateTodo(input request.CreateTodo) error
	GetTodoById(title string) (res int, err error)
	GetOneDetailTodoById(id int) (res model.DataTodo, err error)
	DetailAllTodo() (res []model.DataTodo, err error)
	GetOneDetailTodo(todo_id int) (res model.DataTodo, err error)
	DeleteTodo(id int) error
	UpdateAllDataTodo(requestData request.UpdateTodo) error
}

type repoTodo struct {
	db *sql.DB
}

func NewRepoTodo(db *sql.DB) *repoTodo {
	return &repoTodo{db}
}
func (r *repoTodo) CreateTodo(input request.CreateTodo) error {
	query := `
		insert 
			into 
		todos 
			(
			activity_group_id,
			title,
			priority,
			is_active,
			created_at) 
		values 
			(?,
			 ?,
			 ?,
			 ?,
			NOW())
		`

	Priority := "very-high"

	_, err := r.db.Exec(query, input.ActivityGroupId, input.Title, Priority, input.IsActive)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (r *repoTodo) GetTodoById(title string) (res int, err error) {
	query := `select max(todo_id) from (select todo_id from todos where title = ? order by todo_id limit 1 ) as tbqorder`
	row := r.db.QueryRow(query, title)

	err = row.Scan(&res)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (r *repoTodo) GetOneDetailTodoById(id int) (res model.DataTodo, err error) {
	query := `select todo_id,activity_group_id,title,priority,is_active,created_at,updated_at from todos where todo_id = ?`
	var updatedat sql.NullTime
	row, err := r.db.Query(query, id)
	if err != nil {
		log.Println(err)
		return res, err
	}

	for row.Next() {
		err := row.Scan(&res.Id,
			&res.ActivityGroupId,
			&res.Title,
			&res.Priority,
			&res.IsActive,
			&res.CreateAt,
			&updatedat)
		res.UpdateAt = updatedat.Time

		if err != nil {
			return res, err
		}

	}
	return res, nil
}

func (r *repoTodo) DetailAllTodo() (res []model.DataTodo, err error) {
	query := `select todo_id,activity_group_id,title,priority,is_active,created_at,updated_at from todos`
	var updatedat sql.NullTime
	row, err := r.db.Query(query)
	if err != nil {
		log.Println(err)
		return res, err
	}
	data := model.DataTodo{}
	for row.Next() {
		err := row.Scan(&data.Id,
			&data.ActivityGroupId,
			&data.Title,
			&data.Priority,
			&data.IsActive,
			&data.CreateAt,
			&updatedat)
		data.UpdateAt = updatedat.Time
		if err != nil {
			return res, err
		}
		res = append(res, data)
	}
	return res, nil
}

func (r *repoTodo) GetOneDetailTodo(todo_id int) (res model.DataTodo, err error) {
	query := `select todo_id,activity_group_id,title,priority,is_active,created_at,updated_at from todos where todo_id = ?`
	var updatedat sql.NullTime
	row, err := r.db.Query(query, todo_id)
	if err != nil {
		log.Println(err)
		return res, err
	}

	for row.Next() {
		err := row.Scan(&res.Id,
			&res.ActivityGroupId,
			&res.Title,
			&res.Priority,
			&res.IsActive,
			&res.CreateAt,
			&updatedat)
		res.UpdateAt = updatedat.Time

		if err != nil {
			return res, err
		}

	}
	return res, nil
}

func (r *repoTodo) DeleteTodo(id int) error {
	query := `delete from todos where todo_id = ?`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}
	return nil

}

func (r *repoTodo) UpdateAllDataTodo(requestData request.UpdateTodo) error {
	query := `update todos set title = ?,priority = ?,is_active = ?,updated_at = NOW() where todo_id = ?`

	_, err := r.db.Query(query, requestData.Title, requestData.Priority, requestData.IsActive, requestData.TodoId)
	if err != nil {
		return err
	}
	return nil
}
