package repo

import (
	"belajar/exam-skyshi/service/model"
	"belajar/exam-skyshi/service/model/request"
	"database/sql"
	"log"
)

type RepoActivity interface {
	CreateActivity(input request.CreateActivity) error
	DetailAllActivity() (res []model.Data, err error)
	GetOneDetailActivity(id int) (res model.Data, err error)
	GetActivityId(title string) (res int, err error)
	DeleteActivity(id int) error
	UpdateAllData(requestData request.Activity) error
}

type repoActivity struct {
	db *sql.DB
}

func NewRepoActivity(db *sql.DB) *repoActivity {
	return &repoActivity{db}
}

func (r *repoActivity) CreateActivity(input request.CreateActivity) error {
	query := `
		insert 
			into 
		activities 
			(
			title,
			email,
			created_at) 
		values 
			(?,
			 ?,
			NOW())
		`

	_, err := r.db.Exec(query, input.Title, input.Email)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (r *repoActivity) DetailAllActivity() (res []model.Data, err error) {
	query := `select activity_id,title,email,created_at,updated_at from activities`
	var updatedat sql.NullTime
	row, err := r.db.Query(query)
	if err != nil {
		log.Println(err)
		return res, err
	}
	data := model.Data{}
	for row.Next() {
		err := row.Scan(&data.Id,
			&data.Title,
			&data.Email,
			&data.CreateAt,
			&updatedat)
		data.UpdateAt = updatedat.Time
		if err != nil {
			return res, err
		}
		res = append(res, data)
	}
	return res, nil
}

func (r *repoActivity) GetOneDetailActivity(id int) (res model.Data, err error) {
	query := `select activity_id,title,email,created_at,updated_at from activities where activity_id = ?`
	var updatedat sql.NullTime
	row, err := r.db.Query(query, id)
	if err != nil {
		log.Println(err)
		return res, err
	}

	for row.Next() {
		err := row.Scan(&res.Id,
			&res.Title,
			&res.Email,
			&res.CreateAt,
			&updatedat)
		res.UpdateAt = updatedat.Time

		if err != nil {
			return res, err
		}

	}
	return res, nil
}

func (r *repoActivity) DeleteActivity(id int) error {
	query := `delete from activities where activity_id = ?`
	_, err := r.db.Exec(query, id)
	if err != nil {
		return err
	}
	return nil

}

func (r *repoActivity) GetActivityId(title string) (res int, err error) {
	query := `select max(activity_id) from (select activity_id from activities where title = ? order by activity_id limit 1 ) as tbqorder`
	row := r.db.QueryRow(query, title)

	err = row.Scan(&res)
	if err != nil {
		return res, err
	}

	return res, nil
}

func (r *repoActivity) UpdateAllData(requestData request.Activity) error {
	query := `update activities set title = ?,email = ?,updated_at = NOW() where activity_id = ?`

	_, err := r.db.Query(query, requestData.Title, requestData.Email, requestData.Id)
	if err != nil {
		return err
	}
	return nil
}
