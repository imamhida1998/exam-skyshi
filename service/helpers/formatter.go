package helpers

import "belajar/exam-skyshi/service/model/response"

func FormatDataResponse(Status, Message string, data interface{}) (res response.MessageDataResponse) {
	res.Status = Status
	res.Message = Message
	res.Data = data
	return res
}

func FormatResponse(Status, Message string) (res response.MessageResponse) {
	res.Status = Status
	res.Message = Message

	return res
}
