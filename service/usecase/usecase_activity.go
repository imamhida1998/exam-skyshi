package usecase

import (
	"belajar/exam-skyshi/service/model"
	"belajar/exam-skyshi/service/model/request"
	"belajar/exam-skyshi/service/repo"
	"log"
)

type UsercaseActivity interface {
	CreatedActivty(input request.CreateActivity) (res model.Data, err error)
	GetAllActivity() (res []model.Data, err error)
	GetOneActivityId(input request.ActivityId) (res model.Data, err error)
	DeleteActivity(input request.ActivityId) error
	UpdateActivity(inputUri request.ActivityId, UpdateData request.Activity) error
}

type usecaseActivity struct {
	repo repo.RepoActivity
}

func NewUsecaseActivity(repo repo.RepoActivity) *usecaseActivity {
	return &usecaseActivity{repo}
}

func (a *usecaseActivity) CreatedActivty(input request.CreateActivity) (res model.Data, err error) {
	err = a.repo.CreateActivity(input)
	if err != nil {
		log.Println(err)
		return res, err

	}
	last, err := a.repo.GetActivityId(input.Title)
	if err != nil {
		log.Println(err)
		return res, err

	}
	res, err = a.repo.GetOneDetailActivity(last)
	if err != nil {
		log.Println(err)
		return res, err

	}
	log.Println(res)
	return res, nil
}

func (a *usecaseActivity) GetAllActivity() (res []model.Data, err error) {
	res, err = a.repo.DetailAllActivity()
	if err != nil {
		log.Println(err)
		return res, err

	}
	return res, nil

}

func (a *usecaseActivity) GetOneActivityId(input request.ActivityId) (res model.Data, err error) {
	res, err = a.repo.GetOneDetailActivity(input.Id)
	if err != nil {
		log.Println(err)
		return res, err

	}
	return res, nil

}

func (a *usecaseActivity) DeleteActivity(input request.ActivityId) error {
	err := a.repo.DeleteActivity(input.Id)
	if err != nil {
		log.Println(err)
		return err

	}
	return nil

}

func (a *usecaseActivity) UpdateActivity(inputUri request.ActivityId, UpdateData request.Activity) error {
	res, err := a.repo.GetOneDetailActivity(inputUri.Id)
	if err != nil {
		log.Println(err)
		return err
	}
	if UpdateData.Title == "" {
		UpdateData.Title = res.Title
	}
	if UpdateData.Email == "" {
		UpdateData.Email = res.Email
	}

	UpdateData.Id = res.Id

	err = a.repo.UpdateAllData(UpdateData)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil

}
