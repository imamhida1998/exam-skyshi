package usecase

import (
	"belajar/exam-skyshi/service/model"
	"belajar/exam-skyshi/service/model/request"
	"belajar/exam-skyshi/service/repo"
	"log"
)

type UsercaseTodo interface {
	CreatedTodo(input request.CreateTodo) (res model.DataTodo, err error)
	GetAllTodo() (res []model.DataTodo, err error)
	GetOneTodo(input request.TodoId) (res model.DataTodo, err error)
	DeleteTodo(input request.TodoId) error
	UpdateTodo(inputUri request.TodoId, UpdateData request.UpdateTodo) error
}

type usecaseTodo struct {
	repo repo.RepoTodo
}

func NewUsecaseTodo(repo repo.RepoTodo) *usecaseTodo {
	return &usecaseTodo{repo}
}

func (t *usecaseTodo) CreatedTodo(input request.CreateTodo) (res model.DataTodo, err error) {
	err = t.repo.CreateTodo(input)
	if err != nil {
		log.Println(err)
		return res, err

	}
	last, err := t.repo.GetTodoById(input.Title)
	if err != nil {
		log.Println(err)
		return res, err

	}
	res, err = t.repo.GetOneDetailTodoById(last)
	if err != nil {
		log.Println(err)
		return res, err

	}
	log.Println(res)
	return res, nil
}

func (t *usecaseTodo) GetAllTodo() (res []model.DataTodo, err error) {
	res, err = t.repo.DetailAllTodo()
	if err != nil {
		log.Println(err)
		return res, err

	}
	return res, nil

}

func (a *usecaseTodo) GetOneTodo(input request.TodoId) (res model.DataTodo, err error) {
	res, err = a.repo.GetOneDetailTodo(input.Id)
	if err != nil {
		log.Println(err)
		return res, err

	}
	return res, nil

}

func (t *usecaseTodo) DeleteTodo(input request.TodoId) error {
	err := t.repo.DeleteTodo(input.Id)
	if err != nil {
		log.Println(err)
		return err

	}
	return nil

}

func (t *usecaseTodo) UpdateTodo(inputUri request.TodoId, UpdateData request.UpdateTodo) error {
	res, err := t.repo.GetOneDetailTodo(inputUri.Id)
	if err != nil {
		log.Println(err)
		return err
	}
	if UpdateData.Title == "" {
		UpdateData.Title = res.Title
	}
	if UpdateData.Priority == "" {
		UpdateData.Priority = res.Priority
	}

	if UpdateData.IsActive == false {
		UpdateData.IsActive = res.IsActive
	}

	UpdateData.TodoId = res.Id

	err = t.repo.UpdateAllDataTodo(UpdateData)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil

}
