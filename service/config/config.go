package config

import (
	"belajar/exam-skyshi/service/model"
	"github.com/joho/godotenv"
	"os"
)

type Config struct {
}

func (c *Config) InitEnv() error {
	err := godotenv.Load("app/skyshi.env")
	if err != nil {
		return err
	}
	return err
}

func (c *Config) CatchError(err error) {
	if err != nil {
		panic(any(err))
	}
}

func (c *Config) GetDBConfig() model.DBConfig {
	return model.DBConfig{
		DBName:   os.Getenv("MYSQL_DBNAME"),
		Username: os.Getenv("MYSQL_USER"),
		Password: os.Getenv("MYSQL_PASSWORD"),
		Host:     os.Getenv("MYSQL_HOST"),
		Port:     os.Getenv("MYSQL_PORT"),
	}
}

func (c *Config) GetApp() model.APP {
	return model.APP{
		Port: os.Getenv("APP_PORT"),
	}
}
