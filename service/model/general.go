package model

import "time"

type Data struct {
	Id       int       `json:"id"`
	Title    string    `json:"title"`
	Email    string    `json:"email"`
	CreateAt time.Time `json:"createdAt"`
	UpdateAt time.Time `json:"updatedAt"`
}

type DataTodo struct {
	Id              int       `json:"id"`
	Title           string    `json:"title"`
	ActivityGroupId int       `json:"activity_group_id"`
	IsActive        bool      `json:"is_active"`
	Priority        string    `json:"priority"`
	CreateAt        time.Time `json:"createdAt"`
	UpdateAt        time.Time `json:"updatedAt"`
}
