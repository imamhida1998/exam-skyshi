package model

type DBConfig struct {
	DBName   string
	Username string
	Password string
	Host     string
	Port     string
}

type APP struct {
	Port string
}
