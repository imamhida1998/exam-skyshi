package request

type CreateActivity struct {
	Title string `json:"title"`
	Email string `json:"email"`
}

type ActivityId struct {
	Id int `uri:"id"`
}

type Activity struct {
	Id    int    `json:"id"`
	Title string `json:"title"`
	Email string `json:"email"`
}

type TodoId struct {
	Id int `uri:"id"`
}

type UpdateTodo struct {
	TodoId   int
	Title    string `json:"title"`
	Priority string `json:"priority"`
	IsActive bool   `json:"is_active"`
	Status   string `json:"status"`
}

type CreateTodo struct {
	Title           string `json:"title"`
	ActivityGroupId int    `json:"activity_group_id"`
	IsActive        bool   `json:"is_active"`
}
